<?php

use PHPUnit\Framework\TestCase;
require 'main/Function.php';

class FunctionTest extends TestCase {

    public function testAddReturnsTheCorrectSum() {
        $this->assertEquals(5, add(2, 3));
    }
}