* docker run --rm -v $(pwd):/app composer/composer:latest install --ignore-platform-reqs --no-scripts
* docker run -it --rm --name test-script -v "$PWD":/usr/src/app -w /usr/src/app php:7.2-cli php example.php
* run tests in terminal with docker: docker run -it --rm --name myscript -v "$PWD":/usr/src/app -w /usr/src/app php:7.2-cli ./vendor/bin/phpunit tests
* set up php with docker to run phpunit tests: https://blog.alejandrocelaya.com/2017/02/01/run-phpunit-tests-inside-docker-container-from-phpstorm/
